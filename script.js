function enviar_mensagem(count){
    let input = document.querySelector(".input_messages");
    let message_div = document.createElement("div");
    message_div.classList.add("message");
    let message_text_div = document.createElement("div");
    message_text_div.innerText = input.value;
    input.value = "";
    message_text_div.classList.add("message-text");
    message_div.append(message_text_div);
    let edit_btn = document.createElement("button");
    edit_btn.classList.add("edit-btn");
    edit_btn.innerText = "Editar";
    let delete_btn = document.createElement("button");
    delete_btn.classList.add("delete-btn");
    delete_btn.setAttribute("onclick", "this.parentNode.style.display = 'none';");
    delete_btn.innerText = "Excluir";
    message_div.append(edit_btn);
    message_div.append(delete_btn);
    let sent_messages = document.querySelector(".sent-messages");
    sent_messages.append(message_div);
}

let btn_enviar = document.querySelector("#send-btn");
count = btn_enviar.addEventListener("click", ()=>enviar_mensagem(count));

